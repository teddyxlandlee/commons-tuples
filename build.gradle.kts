import org.objectweb.asm.Opcodes as ops
import org.objectweb.asm.ClassWriter
import java.nio.file.*
import java.time.format.DateTimeFormatter
import java.util.Locale
import java.time.ZonedDateTime

buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath(group = "org.ow2.asm", name = "asm", version = "9.2")
    }
}
plugins {
    java
    //`java-library`
}

fun String.toSlashed() = this.replace('.', '/')

class ModuleInfo(val moduleName: String, val moduleVersion: String? = null, val open: Boolean = false) {
    private val exports: MutableMap<String, Array<out String>?> = mutableMapOf()
    private val requires: MutableMap<String, String?> = mutableMapOf()
    private val requiresTransitive: MutableMap<String, String?> = mutableMapOf()
    private val opens: MutableMap<String, Array<out String>?> = mutableMapOf()
    private val provides: MutableMap<String, Array<out String>?> = mutableMapOf()

    private val requiresList = mutableSetOf<String>()

    fun toByteArray() : ByteArray {
        val cw = ClassWriter(0)
        cw.visit(ops.V9, ops.ACC_MODULE, "module-info",
            null, null, null)
        cw.visitSource("asm-gen:teddyxlandlee", null)
        cw.visitModule(moduleName,
            if (open) ops.ACC_OPEN else 0,
            moduleVersion).run {
                requires.forEach { (module, version) ->
                    visitRequire(module, 0, version)
                }
                requiresTransitive.forEach { (module, version) ->
                    visitRequire(module, ops.ACC_TRANSITIVE, version)
                }
                if ("java.base" !in requiresList)
                    visitRequire("java.base", ops.ACC_MANDATED, null)

                exports.forEach { (pkg, to) ->
                    visitExport(pkg, 0, *(to ?: arrayOf()))
                }
                opens.forEach { (pkg, to) ->
                    visitOpen(pkg, 0, *(to ?: arrayOf()))
                }
                provides.forEach { (spi, impl) ->
                    visitProvide(spi, *(impl ?: arrayOf()))
                }
            }
        cw.visitEnd()
        return cw.toByteArray()
    }

    fun exports(pkg: String, vararg to: String) {
        exports[pkg.toSlashed()] = to
    }

    fun exports(pkg: String) {
        exports[pkg.toSlashed()] = null
    }

    private fun checkReqExists(module: String) {
        if (!requiresList.add(module)) {    // already exists
            requires.remove(module)
            requiresTransitive.remove(module)
        }
    }

    fun requires(module: String, version: String?) {
        checkReqExists(module)
        requires[module] = version
    }

    fun requires(vararg modules: String) {
        modules.forEach(::requires)
    }

    fun requires(module: String) {
        checkReqExists(module)
        requires[module] = null
    }

    fun requiresTransitive(module: String, version: String?) {
        checkReqExists(module)
        requiresTransitive[module] = version
    }

    fun requiresTransitive(vararg modules: String) {
        modules.forEach(::requiresTransitive)
    }

    fun requiresTransitive(module: String) {
        checkReqExists(module)
        requiresTransitive[module] = null
    }

    fun opens(pkg: String, vararg to: String) {
        if (open)
            throw kotlin.RuntimeException("module is open")
        opens[pkg.toSlashed()] = to
    }

    fun opens(pkg: String) {
        if (open)
            throw RuntimeException("module is open")
        opens[pkg.toSlashed()] = null
    }

    fun opens(vararg packages: String) {
        packages.forEach(::opens)
    }

    fun provides(spi: String, vararg impl: String) {
        if (impl.isEmpty()) throw kotlin.RuntimeException("Implementation of $spi should not be empty")
        val impl0 = Array(impl.size) { impl[it].toSlashed() }
        provides[spi.toSlashed()] = impl0
    }

    inline operator fun invoke(action: ModuleInfo.() -> Unit) : ModuleInfo {
        action(this)
        return this
    }
}

group = "io.bitbucket.texle"
version = "0.1.2"

val theModuleInfo = ModuleInfo(
    moduleName="xland.commons.tuples",
    moduleVersion="$version",
    open=true
).invoke {
    exports ("xland.commons.tuples")
    requires ("java.base")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8

    withSourcesJar()
    withJavadocJar()
}

tasks.getByName("compileJava", JavaCompile::class) {
    this.options.encoding = "UTF-8"
    if (JavaVersion.current() >= JavaVersion.VERSION_1_9) {
        this.options.release.set(8)
    }

    doLast {
        val dest = this@getByName.destinationDirectory.asFile.get().toPath()
        val bytes = theModuleInfo.toByteArray()
        dest.resolve(Paths.get("META-INF", "versions", "9", "module-info.class"))
            .run {
                Files.createDirectories(this.parent)
                Files.newOutputStream(this).run {
                    this.write(bytes)
                    this.close()
                }
            }
    }
}

tasks.getByName("jar", Jar::class) {
    this.manifest {
        attributes(mapOf(
            "Multi-Release" to "true",
            "Specification-Title" to "Commons Tuples",
            "Specification-Vendor" to "teddyxlandlee",
            "Specification-Version" to "1",
            "Implementation-Title" to project.name,
            "Implementation-Version" to project.version,
            "Implementation-Vendor" to "teddyxlandlee",
            "Implementation-Timestamp" to DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH)
                .format(ZonedDateTime.now())
        ))
    }
}

tasks.getByName("javadoc", Javadoc::class) {
    this.options {
        encoding = "UTF-8"
        locale = "en_US"

    }
}

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}
