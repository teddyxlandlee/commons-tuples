package xland.commons.tuples;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * <p>A data container in which the object should be either
 * {@link L left} or {@link R right}.</p>
 * <p>You can use {@link #left} or {@link #right} to create
 * an {@code Either} object.</p>
 * <p>Also, use {@link #isLeft()} and {@link #isRight()}
 * in order to check if the object is {@code left} or
 * {@code right}.</p>
 * <p>Getters are {@link #getLeft()} and {@link #getRight()}.
 * Before invoking, please check if the {@code Either}
 * object is {@code left} or {@code right}, or there might
 * be a risk of a {@link NoSuchElementException} thrown.</p>
 * <p>Consumers are {@link #ifLeft} and {@link #ifRight}.</p>
 *
 * <p><b>NOTE</b>: The {@code Either} class is logically
 * sealed, meaning that it's unnecessary to be inherited by
 * other classes than {@link Either.Left} and
 * {@link Either.Right}.</p>
 */
public abstract class Either<L, R> {
    private Either() {}

    public abstract boolean isLeft();
    public abstract boolean isRight();

    public abstract void ifLeft(Consumer<? super L> consumer);
    public abstract void ifRight(Consumer<? super R> consumer);

    /**
     * @return the left value if {@link #isLeft}
     * @throws java.util.NoSuchElementException if not {@link #isLeft()}
     */
    public abstract L getLeft();
    /**
     * @return the right value if {@link #isRight}
     * @throws java.util.NoSuchElementException if not {@link #isRight()}
     */
    public abstract R getRight();

    public static <L, R> Either<L, R> left(L value) {
        return new Left<L, R>(value);
    }

    public static <L, R> Either<L, R> right(R value) {
        return new Right<L, R>(value);
    }

    public abstract Either<R, L> swap();

    private static final class Left<L, R> extends Either<L, R> {
        private final L value;

        Left(L value) {
            this.value = value;
        }

        @Override
        public boolean isLeft() {
            return true;
        }

        @Override
        public boolean isRight() {
            return false;
        }

        @Override
        public void ifLeft(Consumer<? super L> consumer) {
            consumer.accept(value);
        }

        @Override
        public void ifRight(Consumer<? super R> consumer) {}    // ignore

        @Override
        public L getLeft() {
            return value;
        }

        @Override
        public R getRight() {
            throw new NoSuchElementException(this + " is left");
        }

        @Override
        public String toString() {
            return "Either.Left[" + value + ']';
        }

        @Override
        public Either<R, L> swap() {
            return right(this.value);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Left<?, ?> left = (Left<?, ?>) o;
            return Objects.equals(value, left.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }
    }

    private static final class Right<L, R> extends Either<L, R> {
        private final R value;

        Right(R value) {
            this.value = value;
        }

        @Override
        public boolean isLeft() {
            return false;
        }

        @Override
        public boolean isRight() {
            return true;
        }

        @Override
        public void ifLeft(Consumer<? super L> consumer) {}     // ignore

        @Override
        public void ifRight(Consumer<? super R> consumer) {
            consumer.accept(value);
        }

        @Override
        public L getLeft() {
            throw new NoSuchElementException(this + " is right");
        }

        @Override
        public R getRight() {
            return value;
        }

        @Override
        public String toString() {
            return "Either.Right[" + value + ']';
        }

        @Override
        public Either<R, L> swap() {
            return left(this.value);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Right<?, ?> right = (Right<?, ?>) o;
            return Objects.equals(value, right.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }
    }
}
