package xland.commons.tuples;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.List;

/**
 * Similar to {@link Triple}, but left, middle
 * and right have the same type.
 * @see Triple
 * @see Twin
 */
public class Triplets<T> extends Triple<T, T, T> {
    public Triplets(T left, T middle, T right) {
        super(left, middle, right);
    }

    public static <T> Triplets<T> ofTriplets(T left, T middle, T right) {
        return new Triplets<>(left, middle, right);
    }

    @Override
    public Triplets<T> swapLM() {
        return ofTriplets(getMiddle(), getLeft(), getRight());
    }

    @Override
    public Triplets<T> swapMR() {
        return ofTriplets(getLeft(), getRight(), getMiddle());
    }

    @Override
    public Triplets<T> swapLR() {
        return ofTriplets(getRight(), getMiddle(), getLeft());
    }

    public Triplets<T> ofLeftTriplets(T left) {
        return ofTriplets(left, getMiddle(), getRight());
    }

    public Triplets<T> ofMiddleTriplets(T middle) {
        return ofTriplets(getLeft(), middle, getRight());
    }

    public Triplets<T> ofRightTriplets(T right) {
        return ofTriplets(getLeft(), getMiddle(), right);
    }

    @Override
    public Triplets<T> clone() {
        return (Triplets<T>) super.clone();
    }

    public List<T> asList() {
        return new AbstractList<T>() {
            /* @Stable */
            List<T> cache;
            private void loadCache() {
                cache = Arrays.asList(getLeft(), getMiddle(), getRight());
            }

            @Override
            public T get(int index) {
                if (cache == null) loadCache();
                return cache.get(index);
            }

            @Override
            public Object[] toArray() {
                return new Object[] { getLeft(), getMiddle(), getRight() };
            }

            @Override
            @SuppressWarnings("all")
            public <T1> T1[] toArray(T1[] a) {
                if (cache == null) loadCache();
                return cache.toArray(a);
            }

            @Override
            public int indexOf(Object o) {
                if (cache == null) loadCache();
                return cache.indexOf(o);
            }

            @Override
            public int lastIndexOf(Object o) {
                if (cache == null) loadCache();
                return cache.lastIndexOf(o);
            }

            @Override
            public int size() {
                return 3;
            }
        };
    }
}
