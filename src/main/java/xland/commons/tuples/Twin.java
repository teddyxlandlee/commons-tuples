package xland.commons.tuples;

import java.util.AbstractList;
import java.util.List;

/**
 * Similar to {@link Pair}, but left and right have
 * the same type.
 * @see Pair
 * @see Triplets
 */
public class Twin<T> extends Pair<T, T> {
    public Twin(T left, T right) {
        super(left, right);
    }

    public static <T> Twin<T> ofTwin(T left, T right) {
        return new Twin<>(left, right);
    }

    public Twin<T> ofRightTwin(T right) {
        return ofTwin(getLeft(), right);
    }

    public Twin<T> ofLeftTwin(T left) {
        return ofTwin(left, getRight());
    }

    @Override
    public Twin<T> swap() {
        return ofTwin(getRight(), getLeft());
    }

    public List<T> asList() {
        return new AbstractList<T>() {
            @Override
            public T get(int index) {
                if (index == 0) return getLeft();
                if (index == 1) return getRight();
                throw new IndexOutOfBoundsException();
            }

            @Override
            public Object[] toArray() {
                return new Object[] { getLeft(), getRight() };
            }

            @Override
            public int indexOf(Object o) {
                if (o == null) {
                    if (getLeft() == null) return 0;
                    if (getRight() == null) return 1;
                } else {
                    if (o.equals(getLeft())) return 0;
                    if (o.equals(getRight())) return 1;
                }
                return -1;
            }

            @Override
            public int lastIndexOf(Object o) {
                if (o == null) {
                    if (getRight() == null) return 1;
                    if (getLeft() == null) return 0;
                } else {
                    if (o.equals(getRight())) return 1;
                    if (o.equals(getLeft())) return 0;
                }
                return -1;
            }

            @Override
            public int size() {
                return 2;
            }
        };
    }

    @Override
    public Twin<T> clone() {
        return (Twin<T>) super.clone();
    }
}
