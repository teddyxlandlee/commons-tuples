package xland.commons.tuples;

import java.util.Objects;

/**
 * An immutable collection for 3 objects.
 * @see Triplets
 * @see Pair
 */
public class Triple<A, B, C> implements Cloneable {
    private final A left;
    private final B middle;
    private final C right;

    public Triple(A left, B middle, C right) {
        this.left = left;
        this.middle = middle;
        this.right = right;
    }

    public static <A, B, C> Triple<A, B, C> of(A left, B middle, C right) {
        return new Triple<>(left, middle, right);
    }

    public A getLeft() {
        return left;
    }

    public B getMiddle() {
        return middle;
    }

    public C getRight() {
        return right;
    }

    public Triple<B, A, C> swapLM() {
        return of(middle, left, right);
    }

    public Triple<A, C, B> swapMR() {
        return of(left, right, middle);
    }

    public Triple<C, B, A> swapLR() {
        return of(right, middle, left);
    }

    public <U> Triple<U, B, C> ofLeft(U left) {
        return of(left, middle, right);
    }

    public <U> Triple<A, U, C> ofMiddle(U middle) {
        return of(left, middle, right);
    }

    public <U> Triple<A, B, U> ofRight(U right) {
        return of(left, middle, right);
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Triple<A, B, C> clone() {
        try {
            Triple clone = (Triple) super.clone();
            return (Triple<A, B, C>) clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    @Override
    public String toString() {
        return "(" + left + ", " + middle + ", "
                + right + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Triple)) return false;
        Triple<?, ?, ?> triple = (Triple<?, ?, ?>) o;
        return Objects.equals(left, triple.left) && Objects.equals(middle, triple.middle) && Objects.equals(right, triple.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, middle, right);
    }
}
