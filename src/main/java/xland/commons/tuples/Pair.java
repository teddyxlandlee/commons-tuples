package xland.commons.tuples;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Objects;

/**
 * An immutable pair of objects.
 * @see Twin
 * @see Triple
 */
public class Pair<L, R> implements Cloneable {
    private final L left;
    private final R right;

    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    public static <L, R> Pair<L, R> of(L left, R right) {
        return new Pair<>(left, right);
    }

    public L getLeft() {
        return left;
    }

    public R getRight() {
        return right;
    }

    public Map.Entry<L, R> asMapEntry() {
        return new AbstractMap.SimpleImmutableEntry<L, R>(left, right);
    }

    public Pair<R, L> swap() {
        return of(right, left);
    }

    public <U> Pair<L, U> ofRight(U right) {
        return of(left, right);
    }

    public <U> Pair<U, R> ofLeft(U left) {
        return of(left, right);
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Pair<L, R> clone() {
        try {
            Pair clone = (Pair) super.clone();
            return (Pair<L, R>) clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pair)) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(left, pair.left) && Objects.equals(right, pair.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right);
    }

    @Override
    public String toString() {
        return "(" + this.left + ", " + this.right + ')';
    }
}
